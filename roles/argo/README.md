Argo
=========

This role will deploy Argo release 2.3.0. The installation of Argo is within a namespace and not cluster wide. 

This role installs the following pods
1. Argo UI pod
2. Argo Workflow Controller pod

For more information on Argo and components please visit [Argo Github project](https://github.com/argoproj/argo)

Role Variables
--------------

None


Dependencies
------------

There are no dependencies. However, to install Argo set odh_deploy: true in the Custom Resource file.


License
-------

Apache License 2.0 

Author Information
------------------

jnakfour@redhat.com
