<img src="datahub_color_vert-wht-bg.png" alt="Open Data Hub, an AI platform powered by Open Source" title="Open Data Hub, an AI platform powered by Open Source" />

Open Data Hub Ansible Operator
----------
Implementation of the ansible operator used to the deploy the Open Data Hub in an openshift environment

Components
----------
Open Data Hub Core
* Support for S3 Storage via [Rook Ceph Object Storage](http://rook.io)
* [JupyterHub](docs/using-aicoe-jupyterhub.adoc)
* [Spark Operator](https://github.com/radanalyticsio/spark-operator)
* [Monitoring](docs/deploying-monitoring.adoc) with Prometheus & Grafana
* [Seldon](docs/deploying-seldon.adoc)
* [AI-Library](docs/deploying-ai-library.adoc)
* [Argo](docs/deploying-argo.adoc)
* [Kafka](docs/deploying-kafka.adoc)
* [Superset](docs/deploying-superset.adoc)
* [Data Catalog](docs/deploying-data-catalog.adoc) with Hue and Thrift Server (***Tech Preview***)
* Two Sigma [BeakerX](docs/using-beakerx.adoc)

Operator Image available @ https://quay.io/opendatahub/opendatahub-operator
Custom Resource Definition: OpenDataHub

Installation
----------

**Installation of the Open Data Hub Community Operator from the OpenShift OperatorHub portal**

Open Data Hub is available in the OpenShift OperatorHub webui as a community operator.

**Manual Installation using the OpenShift command line interface**

To manually install the operator, see the
[Manual Installation documentation](/docs/manual-installation.adoc).

Directory
----------
* build/ - Dockerfile used to build the operator image
* deploy/ - OpenShift templates for the CRD, RBAC and operator resources
* deploy/olm-catalog - OpenShift catalog and subscription files for the package
* docs/ - Documentation for the operator
* roles/ - Ansible roles used to deploy each component of the Open Data Hub
* playbook.yml - Ansible playbook that manages that orchestrates the deployment of the Open Data Hub within the namespace
* watches.yaml - Yaml that registers the Custom Resources managed by this operator

Tutorials and Guides
--------------------
A full set of tutorials and workshop material can be found in the
[tutorials](/tutorials/) directory.

Simple user guides for interacting with the Open Data Hub can be found in the
[tutorials/user_guides/](/tutorials/user_guides/) directory.

Other Resources
----------
- [opendatahub.io](https://opendatahub.io) - For information on the Open Data Hub project
- [OpenShift OperatorHub](https://docs.openshift.com/container-platform/4.2/operators/olm-understanding-operatorhub.html)
- [Ansible Operator User Guide](https://raw.githubusercontent.com/operator-framework/operator-sdk/master/doc/ansible/user-guide.md)
- [Operator SDK](https://github.com/operator-framework/operator-sdk)
- [Operator Lifecycle Manager](https://github.com/operator-framework/operator-lifecycle-manager)
